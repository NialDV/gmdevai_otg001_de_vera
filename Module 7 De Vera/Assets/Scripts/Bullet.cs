﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public GameObject explosion;
	public int bulletDmg;
	
	void OnCollisionEnter(Collision col)
    {
    	GameObject e = Instantiate(explosion, this.transform.position, Quaternion.identity);
		if (col.gameObject.GetComponent<Unit>())
			col.gameObject.GetComponent<Unit>().takeDamage(bulletDmg);
		Destroy(e, 1.5f);
		Destroy(this.gameObject);
    }
}
