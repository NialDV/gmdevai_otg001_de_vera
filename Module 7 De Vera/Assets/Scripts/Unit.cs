﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Unit : MonoBehaviour
{
    public int Health = 100;
    public virtual void takeDamage(int damage)
    {
        if (Health > 0)
            Health -= damage;
        if (Health <= 0)
            Destroy(this.gameObject);
    }
}
